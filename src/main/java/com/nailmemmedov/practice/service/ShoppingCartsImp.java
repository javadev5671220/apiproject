package com.nailmemmedov.practice.service;


import com.nailmemmedov.practice.dto.ShoppingCartsRequestDto;
import com.nailmemmedov.practice.model.Product;
import com.nailmemmedov.practice.model.ShoppingCarts;
import com.nailmemmedov.practice.repository.ProductRepository;
import com.nailmemmedov.practice.repository.ShoppingCartsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShoppingCartsImp implements ShoppingCartsService {
    private final ProductRepository productRepository;
    private final ShoppingCartsRepository shoppingCartsRepository;

    @Override
    public Long createShoppingCart(ShoppingCartsRequestDto dto) {
        ShoppingCarts shoppingCarts = ShoppingCarts.builder()
                .name(dto.getName())
                .build();
        shoppingCartsRepository.save(shoppingCarts);
        return shoppingCarts.getId();
    }

    @Override
    public Long addProductToCart(Long shoppingCartId, Long productId){

        ShoppingCarts shoppingCarts = shoppingCartsRepository.getReferenceById(shoppingCartId);
        Product product = productRepository.getReferenceById(productId);

        shoppingCarts.getProducts().add(product);
        shoppingCartsRepository.save(shoppingCarts);

        return productId;
    }

    @Override
    public void removeProductFromCart(Long shoppingCartId, Long productId) {
        Optional<ShoppingCarts> shoppingCartsOpt = shoppingCartsRepository.findById(shoppingCartId);

        if (shoppingCartsOpt.isPresent()) {
            shoppingCartsOpt.get().getProducts().removeIf((prroduct) -> prroduct.getId().equals(productId));
        }
        shoppingCartsRepository.save(shoppingCartsOpt.get());
    }

    @Override
    public ShoppingCarts getShoppingById(Long shoppingCartId) {
        ShoppingCarts allCustom = shoppingCartsRepository.findAllCustom(shoppingCartId);
        return allCustom;
    }
}
