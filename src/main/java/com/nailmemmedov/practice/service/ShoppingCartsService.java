package com.nailmemmedov.practice.service;

import com.nailmemmedov.practice.dto.ShoppingCartsRequestDto;
import com.nailmemmedov.practice.model.ShoppingCarts;

public interface ShoppingCartsService {
    Long createShoppingCart(ShoppingCartsRequestDto dto);
    Long addProductToCart(Long shoppingCartId, Long productId);
    void removeProductFromCart(Long shoppingCartId, Long productId);
    ShoppingCarts getShoppingById(Long shoppingCartId);
}
