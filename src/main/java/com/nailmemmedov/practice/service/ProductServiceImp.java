package com.nailmemmedov.practice.service;

import com.nailmemmedov.practice.dto.ProductRequestDto;
import com.nailmemmedov.practice.dto.ProductResponseDto;
import com.nailmemmedov.practice.model.Category;
import com.nailmemmedov.practice.model.Product;
import com.nailmemmedov.practice.repository.CategoryRepository;
import com.nailmemmedov.practice.repository.ProductProjection;
import com.nailmemmedov.practice.repository.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImp implements ProductService{

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    // Main
    @Override
    public List<ProductResponseDto> getPriceFilterAndAll(Integer priceTo, Integer priceFrom) {
        if(priceTo != null && priceFrom != null){
            List<Product> productList =  productRepository.findByPriceBetweenAndPriceGreaterThan(priceTo,priceFrom);
            List<ProductResponseDto> response01 = productList.stream()
                    .map(product -> ProductResponseDto.builder()
                            .id(product.getId())
                            .name(product.getName())
                            .price(product.getPrice())
                            .description(product.getDescription())
                            .category(product.getCategory())
                            .productDetail(product.getProductDetail())
                            .build())
                    .collect(Collectors.toList());
            return response01;
        }

        List<Product> productList = productRepository.findAll();
        List<ProductResponseDto> response02 = productList.stream()
                .map(product -> ProductResponseDto.builder()
                        .id(product.getId())
                        .name(product.getName())
                        .price(product.getPrice())
                        .description(product.getDescription())
                        .category(product.getCategory())
                        .productDetail(product.getProductDetail())
                        .build())
                .collect(Collectors.toList());

        return response02;
    }

    @Override
    public Map<String, Integer> getCountByCategories() {
        List<ProductProjection> projectionList = productRepository.findProductCountByCategory();
        Map<String,Integer> categoryCountMap = projectionList.stream()
                .collect(Collectors.toMap(
                        p -> p.getCategory().toString(),
                        ProductProjection::getProductCount
                ));

        return categoryCountMap;
    }

    // Additional
    @Override
    public ProductResponseDto get(Long id) {
        Product product = productRepository.findById(id).orElseThrow(()-> new RuntimeException());
        ProductResponseDto responce = ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .description(product.getDescription())
                .category(product.getCategory())
                .productDetail((product.getProductDetail()))
                .build();
        return responce;
    }

    @Override
    public void delete(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()){
            productRepository.deleteById(id);
        }
    }

    @Override
    public ProductResponseDto update(Long id, ProductRequestDto dto) {
        Product product = productRepository.findById(id).orElseThrow(()-> new RuntimeException());


        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setDescription(dto.getDescription());
        product.setCategory(dto.getCategory());
        product.setProductDetail(dto.getProductDetail());

        productRepository.save(product);
        return ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .description(product.getDescription())
                .category(product.getCategory())
                .productDetail(product.getProductDetail())
                .build();
    }

    @Override
    public long create(ProductRequestDto dto) {
        Optional<Category> isExisting = categoryRepository.findCategoryIdByName(dto.getCategory().getName());
        if(isExisting.isPresent()){
            Product product = Product.builder()
                .name(dto.getName())
                .price(dto.getPrice())
                .description(dto.getDescription())
                .category(isExisting.get())
                .productDetail(dto.getProductDetail())
                .build();
            productRepository.save(product);
            return product.getId();

        } else {
            Category category = Category.builder()
                    .name(dto.getCategory().getName())
                    .build();

            Product product = Product.builder()
                    .name(dto.getName())
                    .price(dto.getPrice())
                    .description(dto.getDescription())
                    .category(category)
                    .productDetail(dto.getProductDetail())
                    .build();
            productRepository.save(product);

            return product.getId();
        }

    }
}
