package com.nailmemmedov.practice.service;

import com.nailmemmedov.practice.dto.UserRequestDto;
import com.nailmemmedov.practice.dto.UserResponseDto;
import com.nailmemmedov.practice.generispec.SearchCriteria;

import java.util.List;

public interface UserService {
    // Specification
    List<UserResponseDto> getUserWtSpecification(String  isActive);

    // Dynamic query
    List<UserResponseDto> getUserWtDynamicQuery(List<SearchCriteria> criteria);

    UserResponseDto get(Long id);
    long create(UserRequestDto dto);
    UserResponseDto update(Long id, UserRequestDto dto);
    void delete(Long id);
}
