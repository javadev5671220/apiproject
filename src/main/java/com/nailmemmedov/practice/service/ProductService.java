package com.nailmemmedov.practice.service;

import com.nailmemmedov.practice.dto.ProductRequestDto;
import com.nailmemmedov.practice.dto.ProductResponseDto;

import java.util.List;
import java.util.Map;

public interface ProductService {
    // Additional
    ProductResponseDto get(Long id);
    void delete(Long id);
    ProductResponseDto update(Long id, ProductRequestDto dto);
    long create(ProductRequestDto dto);

    // Main
    List<ProductResponseDto> getPriceFilterAndAll(Integer priceTo, Integer priceFrom);
    Map<String,Integer> getCountByCategories();
}
