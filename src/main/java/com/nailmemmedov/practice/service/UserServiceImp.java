package com.nailmemmedov.practice.service;

import com.nailmemmedov.practice.dto.UserRequestDto;
import com.nailmemmedov.practice.dto.UserResponseDto;
import com.nailmemmedov.practice.generispec.SearchCriteria;
import com.nailmemmedov.practice.generispec.UserSpecification;
import com.nailmemmedov.practice.model.Authority;
import com.nailmemmedov.practice.model.User;
import com.nailmemmedov.practice.repository.UserRepository;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService {
    private final UserRepository userRepository;

    // Specification --------------------------------------------------------------------------------------------------
    @Override
    public List<UserResponseDto> getUserWtSpecification(String isActive) {
        Specification<User> spec01 = ((root, query, criteriaBuilder) -> (criteriaBuilder.equal(root.get("active"), isActive)));
        Specification<User> spec02 = (((root, query, criteriaBuilder) -> {
            Join<User, Authority> join = root.join("authority", JoinType.INNER);
            return criteriaBuilder.equal(join.get("role"),"ADMIN");
        }));


        List<User> users = userRepository.findAll(spec01.and(spec02));
        List<UserResponseDto> dtos = new ArrayList<>();

        users.forEach(user -> {
            UserResponseDto dto = UserResponseDto.builder()
                    .name(user.getName())
                    .authorities(user.getAuthorities())
                    .email(user.getEmail())
                    .password(user.getPassword())
                    .build();
            dtos.add(dto);
        });

        return dtos;
    }

    // Dynamic Query --------------------------------------------------------------------------------------------------
    @Override
    public List<UserResponseDto> getUserWtDynamicQuery(List<SearchCriteria> criterias) {

        UserSpecification userSpecification = new UserSpecification();

        for (SearchCriteria criteria : criterias) {
            userSpecification.add(criteria);
        }

        List<User> users = userRepository.findAll(userSpecification);

        List<UserResponseDto> dtos = new ArrayList<>();
        users.forEach(user -> {
            UserResponseDto dto = UserResponseDto.builder()
                    .name(user.getName())
                    .authorities(user.getAuthorities())
                    .email(user.getEmail())
                    .password(user.getPassword())
                    .build();
            dtos.add(dto);
        });

        return dtos;
    }


    // Main -----------------------------------------------------------------------------------------------------------
    @Override
    public UserResponseDto get(Long id) {

        User user = userRepository.findById(id).orElseThrow(()-> new RuntimeException("User not found"));

        UserResponseDto userResponseDto = UserResponseDto.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .authorities(user.getAuthorities())
                .build();
        return userResponseDto;
    }

    @Override
    public long create(UserRequestDto dto) {

        User user = User.builder()
                .name(dto.getName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .authorities(dto.getAuthorities())
                .build();
        userRepository.save(user);

        return user.getId();
    }

    @Override
    public UserResponseDto update(Long id, UserRequestDto dto) {
        User user = userRepository.findById(id).orElseThrow(()-> new RuntimeException("User not found"));

        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setAuthorities(dto.getAuthorities());

        userRepository.save(user);

        return UserResponseDto.builder()
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .authorities(user.getAuthorities())
                .build();
    }

    @Override
    public void delete(Long id) {
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()) {
            userRepository.deleteById(id);
        }
    }
}
