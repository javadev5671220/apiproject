package com.nailmemmedov.practice.repository;

import com.nailmemmedov.practice.model.Account;
import com.nailmemmedov.practice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long>, JpaSpecificationExecutor<Product> {
    @Query(value = "SELECT p FROM Product p WHERE p.price >= :priceTo AND  p.price <= :priceFrom")
    List<Product> findByPriceBetweenAndPriceGreaterThan(Integer priceTo, Integer priceFrom);

    @Query(value = "select c.name as category,count(c.name) as productCount from Category c join Product p on c.id = p.category.id group by c.name")
    List<ProductProjection> findProductCountByCategory();
}
