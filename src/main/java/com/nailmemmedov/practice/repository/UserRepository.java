package com.nailmemmedov.practice.repository;

import com.nailmemmedov.practice.model.Account;
import com.nailmemmedov.practice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
}
