package com.nailmemmedov.practice.repository;

import com.nailmemmedov.practice.model.Account;
import com.nailmemmedov.practice.model.ShoppingCarts;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ShoppingCartsRepository extends JpaRepository<ShoppingCarts,Long>, JpaSpecificationExecutor<ShoppingCarts> {

    @Query(nativeQuery = false, value = "SELECT sc FROM ShoppingCarts sc JOIN FETCH sc.products WHERE sc.id = :id")
    ShoppingCarts findAllCustom(Long id);
}
