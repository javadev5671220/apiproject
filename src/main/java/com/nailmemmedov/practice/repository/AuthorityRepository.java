package com.nailmemmedov.practice.repository;

import com.nailmemmedov.practice.model.Account;
import com.nailmemmedov.practice.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AuthorityRepository extends JpaRepository<Authority, Long>, JpaSpecificationExecutor<Authority> {

}
