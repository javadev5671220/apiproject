package com.nailmemmedov.practice.repository;


import com.nailmemmedov.practice.category.CategoriesNames;
import com.nailmemmedov.practice.model.Account;
import com.nailmemmedov.practice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;


import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long>, JpaSpecificationExecutor<Category> {
    @Query(value = "select c from Category c where c.name = :name")
    Optional<Category> findCategoryIdByName(CategoriesNames name);
}
