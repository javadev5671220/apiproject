package com.nailmemmedov.practice.repository;

import com.nailmemmedov.practice.category.CategoriesNames;

public interface ProductProjection {
    CategoriesNames getCategory();
    Integer getProductCount();
}