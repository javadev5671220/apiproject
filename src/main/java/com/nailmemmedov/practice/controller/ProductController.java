package com.nailmemmedov.practice.controller;


import com.nailmemmedov.practice.dto.ProductRequestDto;
import com.nailmemmedov.practice.dto.ProductResponseDto;
import com.nailmemmedov.practice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;


    // Main
    @GetMapping("/price")
    public ResponseEntity<List<ProductResponseDto>> getPriceFilterAndAll(@Param("priceTo") Integer priceTo,@Param("priceFrom") Integer priceFrom){
        List<ProductResponseDto> response = productService.getPriceFilterAndAll(priceTo,priceFrom);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/count")
    public ResponseEntity<Map<String,Integer>> getCountProductByCategories(){
        Map<String,Integer> response = productService.getCountByCategories();
        return ResponseEntity.ok(response);
    }



    // Additional
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto> get(@PathVariable Long id) {
        ProductResponseDto productResponseDto = productService.get(id);
        return ResponseEntity.ok(productResponseDto);
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody ProductRequestDto dto){
        long id = productService.create(dto);
        return ResponseEntity.created(URI.create("products/" + id)).build();
    }

    @PutMapping("{id}")
    public ResponseEntity<ProductResponseDto> update(@PathVariable Long id, @RequestBody ProductRequestDto dto){
        ProductResponseDto productResponseDto = productService.update(id, dto);
        return ResponseEntity.ok(productResponseDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        productService.delete(id);
        return ResponseEntity.ok().build();
    }



}
