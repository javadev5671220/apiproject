package com.nailmemmedov.practice.controller;

import com.nailmemmedov.practice.dto.UserRequestDto;
import com.nailmemmedov.practice.dto.UserResponseDto;
import com.nailmemmedov.practice.generispec.SearchCriteria;
import com.nailmemmedov.practice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    // Specification --------------------------------------------------------------------------------------------------
    @GetMapping("/specification")
    public ResponseEntity<List<UserResponseDto>> getUserWtSpecificationApi(@RequestBody String isActive){
        List<UserResponseDto> userWtSpecification = userService.getUserWtSpecification(isActive);
        return ResponseEntity.ok(userWtSpecification);
    }

    // Dynamic Query --------------------------------------------------------------------------------------------------
    @GetMapping("/dynamic-query")
    public ResponseEntity<List<UserResponseDto>> getUserWtDynamicQueryApi(@RequestBody List<SearchCriteria> criterias){
        List<UserResponseDto> userWtDynamicQuery = userService.getUserWtDynamicQuery(criterias);
        return ResponseEntity.ok(userWtDynamicQuery);
    }


    // Main -----------------------------------------------------------------------------------------------------------
    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> get(@PathVariable Long id) {
        UserResponseDto userResponseDto = userService.get(id);
        return ResponseEntity.ok(userResponseDto);
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody UserRequestDto userRequestDto) {
        long id = userService.create(userRequestDto);
        return ResponseEntity.created(URI.create("users/" + id)).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("{id}")
    public ResponseEntity<UserResponseDto> update(@PathVariable Long id, @RequestBody UserRequestDto userRequestDto) {
        UserResponseDto userResponseDto = userService.update(id, userRequestDto);
        return ResponseEntity.ok(userResponseDto);
    }
 }
