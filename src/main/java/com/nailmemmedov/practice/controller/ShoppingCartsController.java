package com.nailmemmedov.practice.controller;

import com.nailmemmedov.practice.dto.ShoppingCartsRequestDto;
import com.nailmemmedov.practice.model.ShoppingCarts;
import com.nailmemmedov.practice.service.ShoppingCartsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Map;

@RestController
@RequestMapping("/shopping-carts")
@RequiredArgsConstructor
public class ShoppingCartsController {

    private final ShoppingCartsService shoppingCartsService;

    @PostMapping
    public ResponseEntity<Void> createShoppingCartMeth(@RequestBody ShoppingCartsRequestDto dto) {
        long id = shoppingCartsService.createShoppingCart(dto);
        return ResponseEntity.created(URI.create("shopping-carts/" + id)).build();
    }

    @PutMapping("/{shoppingCartId}/products")
    public ResponseEntity<Void> addProductToCartMeth(@PathVariable Long shoppingCartId, @RequestBody Map<String,Long> requestBody) {
        Long productId = requestBody.get("productId");
        shoppingCartsService.addProductToCart(shoppingCartId, productId);
        return ResponseEntity.created(URI.create("shopping-carts/" + shoppingCartId + "products/" + productId)).build();
    }

    @DeleteMapping("/{shoppingCartsId}/products/{productId}")
    public ResponseEntity<Void> deleteProductFromCartMeth(@PathVariable Long shoppingCartsId, @PathVariable Long productId){
        shoppingCartsService.removeProductFromCart(shoppingCartsId,productId);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/{shoppingCartsId}")
    public ResponseEntity<ShoppingCarts> getAllShoppingCartsMeth(@PathVariable Long shoppingCartsId){
        ShoppingCarts shoppingById = shoppingCartsService.getShoppingById(shoppingCartsId);
        return ResponseEntity.ok(shoppingById);
    }

}
