package com.nailmemmedov.practice.model;

public enum Role {
    USER,
    ADMIN,
    MANAGER
}
