package com.nailmemmedov.practice.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "authority")
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Enumerated(EnumType.STRING)
    Role role;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "authority_users",
            joinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id") ,
            inverseJoinColumns =@JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    Set<User> users = new HashSet<>();
}
