package com.nailmemmedov.practice.dto;

import com.nailmemmedov.practice.model.Authority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRequestDto {
    String name;
    String email;
    String password;
    Set<Authority> authorities;
}
