package com.nailmemmedov.practice.dto;

import com.nailmemmedov.practice.model.Category;
import com.nailmemmedov.practice.model.ProductDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductRequestDto {
    String name;
    Integer price;
    String description;
    Category category;
    ProductDetail productDetail;
}
